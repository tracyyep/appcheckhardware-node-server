# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This is Async event AppNodeLoopServer - it will fire an event every X seconds, using Asynchronous messages . 

* Version
V01 - Initial

### How do I get set up? ###

* Summary
Read Quick Summary above.

* Configuration
There is a config file to set up any config data that you want.
It is in the file config.app.json

* Dependencies
All these files depended on each other, do not remove any file.

* Database configuration
There is no database yet.

* How to run 
To run this type : "node AppNodeLoopServer.js"

* How to change the code 
To change the code , search for Codechange #1, Codechange #2, Codechange #3. Just change the code in these 3 places, and watch your function gets called for that event that you just introduce.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Send email to tracy.ta@gmail.com