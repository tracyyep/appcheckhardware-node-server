// eventListen.js
// This is required by the AppLoop server or the Ipc Loop Server


/////////////// Setting up EventEmitter with callback ///////////////
const EventEmitter = require('events');
const util = require('util');

function MyEmitter() {
   EventEmitter.call(this);
}
util.inherits(MyEmitter, EventEmitter);

/////////////// Setup Error catching - use an unknown event ///////////////
process.on('uncaughtException', (err) => {
    console.log( "Error raised. Error calling emit with unknown event");
});

// Testing it
//myEmitter.emit('erorevent', new Error('whoop'));

/////////////// Setup data for eventList - and AsyncEventList/////////
const eventList = 
  [
    {
      "name": "EventAA" ,
      "dataidx": 1,
      "reserved": 37
    },
    {
      "name": "EventBB" ,
      "dataidx": 2,
      "reserved": 37
    },
    {
      "name": "EventCC" ,
      "dataidx": 3,
      "reserved": 37
    },
    {
      "name": "EventIpcCheckHardwareStatus" ,
      "dataidx": 4,
      "reserved": 37
    }
  ]
;

const eventFuncList = [];

eventFuncList.push(func_for_EventAA);
eventFuncList.push(func_for_EventBB);
eventFuncList.push(func_for_EventCC);
eventFuncList.push(func_for_IpcCheckHardwareStatus);

////////////// Setting up for Async Events ////////////////
const asyncEventList = require('./asyncEventList.js');

//console.log(asyncEventList);

const funcAsyncList = require('./asyncFuncList.js');

//console.log(funcAsyncList);

const eventAsyncFuncList = [];

eventAsyncFuncList.push(funcAsyncList.func_Async_EventAA);
eventAsyncFuncList.push(funcAsyncList.func_Async_EventBB);
eventAsyncFuncList.push(funcAsyncList.func_Async_EventCC);
eventAsyncFuncList.push(funcAsyncList.func_Async_EventAppCheckHardware);


/////////////// Create Event Listener /////////
//////////////  event - Emit event 1 ///////////////


const myEmitter = new MyEmitter();

myEmitter.on('event', () => {
     console.log('EVENT 1----------');
     console.log('an event occurred');
});

console.log('Max listeners: ');
console.log(myEmitter.getMaxListeners());


/////////////// eventSync - Emit event with params///////////////

var myEventList = eventList;
var myEventFunc = eventFuncList;

myEmitter.on('eventSync', function(a,b)  {
     console.log('\tEVENT Sync ----------');
     console.log('\t-- param - Event name : ', a);
     console.log('\t-- param : ', b);
     //console.log(a,b, this);
     
     // Call function according to aparam name
     var eventmatchcount = 0;

     for ( var i=0; i< myEventList.length; i++ ) {

         myEventList[i].dataidx ;
         myEventList[i].name ;

         if ( myEventList[i].name  === a ) {
             myEventFunc[i]();
             eventmatchcount++;
         }
            
     }

     console.log("\teventmatchcount:");
     console.log("\t" + eventmatchcount);

});


/////////////// eventAsync - Emit event Asynchronously///////////////

var myAsyncEventFunc = eventAsyncFuncList;

var myAsyncEventList = asyncEventList;

console.log("AsyncFunclist count"); console.log(eventAsyncFuncList.length);
console.log("AsyncEventlist count"); console.log(myAsyncEventList.length);

myEmitter.on('eventAsync', (a, b) => {
  setImmediate(() => {
     console.log('\tEVENT Async----------');
     console.log('\t-- param - Event name : ', a);
     console.log('\t-- param : ', b);
  });

  // Call function according to aparam name
  var eventmatchcount = 0;
  for ( var i=0; i< myAsyncEventList.length; i++ ) {

         console.log( myAsyncEventList[i].dataidx) ;
         console.log( myAsyncEventList[i].name) ;

         if ( myAsyncEventList[i].name  === a ) {
             myAsyncEventFunc[i]();
             eventmatchcount++;
         }
            
  }

  console.log("matchcount is "); console.log(eventmatchcount);
});

////////// DO NOT CHANGE THIS LINE ///////////
// IMPORTANT -- Export this Emitter
module.exports = myEmitter;

/////////////// All functions to be called when event is matched ///////////////
///////////////////////////////////// Functions for sync Events /////////////////
function   func_for_IpcCheckHardwareStatus() {
     console.log('Called func_for_IpcCheckHardwareStatus');
}

function func_for_EventCC() {
     console.log('iiiiiiiiiiCalled func_for_EventCC');
}

function func_for_EventBB() {
     console.log('bbbbbbbbbbCalled func_for_EventBB');
}

function func_for_EventAA() {
     console.log('Called func_for_EventAA');
}

///////////////////////////////////// Functions for Async Events /////////////////

function func_for_Async_EventAA( ) {
     console.log('Called func_for_Async_EventAA');
}

function func_for_Async_EventBB() { 
     console.log('Called func_for_Async_EventBB');
}

function func_for_Async_EventCC() {
     console.log('mmmmmmCalled func_for_Async_EventCC');
}

function func_for_Async_AppCheckHardwareStatus() {
     console.log('Called func_for_Async_AppCheckHardwareStatus');
} 


