// EventEmit.js
// This event Emit is for Synchronous - Blocking 
// When emit an event a signal eventSync will be sent to the node server -
// And the appropriate function call will be called by the server
// Based on the event name "eventSync" and its parameters "eventCC" and "bparam"
var myEmit = require ('./eventListen.js');


// Emitting event
//myEmit.emit('event');

/////////////// Emit event with params///////////////

// Emitting event
myEmit.emit('eventSync','eventCC', 'bparam');
