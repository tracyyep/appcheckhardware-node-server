// AppNodeLoopServer.js
//

/////////////- Reading Config file ////////////

var fs = require('fs');
var file = __dirname + '/config.app.json';

// Required for event Listener
var myEmit = require ('./eventListen.js');

fs.readFile(file, 'utf8', function (err, data) {
  
  if (err) {
    console.log('Error: ' + err.stack);
    return;
  }

  try {
     data = JSON.parse(data);
  } catch(e) {
     throw new Error(" Data in config.app.json is corrupted ");
  }

  console.dir(data);
});

///////////- Calling App  ////////////////

function callAppAsync() {
    console.log(" Call App Async is called ");
    emittingEvent();

}

////////////  Main //////////////////
//// Calling func callAppAsync every 2 seconds //////// 

var t = setInterval( callAppAsync, 2000);

////////////// Don't change anything above this line ////////////////

////////////////// Want to create an event - change the second param to the event name that you want ///////////
//////// Never change the first param - because that is the type of event you want to create ////////////////
//////// First Param: is the event type - Async or Sync Event
//////// Second param: is event name  - 'AsyncEventAA' eventname matching with asyncEventList.js//////////////////////
//////// Third Param: is any param you want to pass in //////////////
//////// This case: it is an asynchronous event  /////////////////

function emittingEvent()
{
  // Codechange place #1 - Emit an event here 
  /////////////// Emit event Asynchronously///////////////

  myEmit.emit('eventAsync' , 'AsyncEventAA', 'bparam');
  myEmit.emit('eventAsync' , 'EventAppCheckHardwareStatus', 'bparam');

}

