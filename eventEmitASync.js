// eventEmitASync.js
// This event will emit a signal eventAsync - and the signal will be captured by a node server
// This event signal is non-blocking - Asynchronous - 
// When signal is captured by server - the appropriate function will be called based on
// the following:
// the type of event -  "eventAsync" [ its asynchronous ]
// the name of event -  "AsyncEventCC" 
// the first parameter  -  "bparam"
var myEmit = require ('./eventListen.js');

/////////////// Emit event Asynchronously///////////////

myEmit.emit('eventAsync' , 'AsyncEventCC', 'bparam');

